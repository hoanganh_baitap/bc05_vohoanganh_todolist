import { Todo } from "./model.js";
const BASE_URL = "https://62eb6d51705264f263d8473c.mockapi.io/todoApp";
// 
let arrTodo = [];

// BẬT TẮT LOADING
const batLoading = () => {
    document.getElementById('loading').style.display = "flex";
}
const tatLoading = () => {
    document.getElementById('loading').style.display = "none";
}


// LẤY DANH SÁCH TODO TỪ API
const getTodo = () => {
    batLoading();
    axios({
        url: `${BASE_URL}`,
        method: "GET"
    }).then((res) => {
        tatLoading();
        let myData = res.data;
        ////!: render ra giao diện
        xuLyRender(myData);
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
}
getTodo();


// THÊM TODO
export const themTodo = () => {
    const ten = xuLyTen();
    let newTodo = new Todo(ten, false);
    handleCreateAPI(newTodo);
    resetInput();
}
const handleCreateAPI = (newTodo) => {
    batLoading();
    axios({
        url: `${BASE_URL} `,
        method: "POST",
        data: newTodo
    }).then((res) => {
        getTodo();
    }).catch((err) => {
        console.log('err: ', err);
    });
}
const xuLyTen = () => {
    const newName = document.getElementById('newTask').value;
    return newName;
}


// XÓA TODO
export const xoaTodo = (todoID) => {
    batLoading();
    axios({
        url: `${BASE_URL}/${todoID}`,
        method: "DELETE"
    }).then((res) => {
        getTodo();
    }).catch((err) => {
        console.log('err: ', err);
    });
}


// UPDATE TODO
export const suaTodo = (todoID) => {
    // show thông tin của lên form
    showThongTinLenForm(todoID);
    thayDoiButtonUpdate(todoID);

}
const thayDoiButtonUpdate = (todoID) => {
    setDisplayNone('#addItem');
    // ! gắn button sửa vào parentElement
    attackButton();
    // ! gắn hàm update cho button sửa
    let btnEdit = document.getElementById('editItem');
    btnEdit.addEventListener('click', () => {
        let editInput = document.getElementById('newTask').value;
        handleUpdate(todoID, editInput);
    })
}
const handleUpdate = (todoID, editInput) => {
    batLoading();
    // ! gọi API update
    axios({
        url: `${BASE_URL}/${todoID}`,
        method: "PUT",
        data: {
            name: editInput,
        }
    }).then((res) => {
        getTodo();
    }).catch((err) => {
        console.log(err);
    })
    setDisplay('#addItem');
    removeButton();
    resetInput();
}
const resetInput = () => {
    document.getElementById('newTask').value = '';
}
const setDisplayNone = (selector) => {
    document.querySelector(`${selector}`).classList.add("d-none")
}
const setDisplay = (selector) => {
    document.querySelector(`${selector}`).classList.remove("d-none")
}
const attackButton = () => {
    let parentElement = document.querySelector('.card__add');
    let editButton = `
    <button id="editItem">
    <i class="fa fa-save"></i>
    </button>
    `
    parentElement.innerHTML += editButton;
}
const removeButton = () => {
    let parentElement = document.querySelector('.card__add');
    let child = parentElement.lastElementChild;
    parentElement.removeChild(child);

}
const showThongTinLenForm = (todoID) => {
    batLoading();
    axios({
        url: `${BASE_URL}/${todoID}`,
        method: "GET"
    }).then((res) => {
        tatLoading();
        const todoObject = res.data;
        dayLen(todoObject);
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
}
const dayLen = (ObjectCanLay) => {
    document.getElementById('newTask').value = ObjectCanLay.name;
}
const xuLyRender = (arrTodo) => {
    let rows1 = ``;
    let rows2 = ``;
    let tasksEl1 = document.getElementById('completed');
    let tasksEl2 = document.getElementById('todo');

    arrTodo.forEach((todo) => {
        if (todo.completed) {

            let row =
                `
        <li class="d-flex" >
            <div class="flex-grow-1"> 
                <span>${todo.name}</span>
            </div>
            <div class="flex-grow-0">
                <span
                onclick = "suaTodo(${todo.id})"
                class="btn">
                <i class="fa fa-user-edit"></i>
                </span>
                <span onclick="xoaTodo(${todo.id})" class="btn">
                    <i class="fa fa-trash-alt "></i>
                </span>
                <span
                onclick= "checkTodo(${todo.id})"
                class="btn">
                    <i class="fa fa-check"></i>
                </span>
            </div>
            </li >
    `
            rows1 += row;

        } else {

            let row =
                `
    <li class="d-flex" >
        <div class="flex-grow-1"> 
            <span>${todo.name}</span>
        </div>
        <div class="flex-grow-0">
            <span
            onclick = "suaTodo(${todo.id})"
            class="btn">
            <i class="fa fa-user-edit"></i>
            </span>
            <span onclick="xoaTodo(${todo.id})" class="btn">
                <i class="fa fa-trash-alt "></i>
            </span>
            <span class="btn"  onclick= "checkTodo(${todo.id})">
                <i class="fa fa-check"></i>
            </span>

        </div>
        </li >
    `
            rows2 += row;

        }

    }

    )
    tasksEl1.innerHTML = rows1;
    tasksEl2.innerHTML = rows2;
}
// ! KIỂM TRA TODO
export const checkTodo = (todoID) => {
    batLoading();
    axios({
        url: `${BASE_URL}/${todoID}`,
        method: "GET"
    }).then((res) => {
        const todoObject = res.data;
        axios({
            url: `${BASE_URL}/${todoID}`,
            method: "PUT",
            data: {
                completed: !todoObject.completed,
            }
        }).then((res) => {
            getTodo();
        }).catch((err) => {
            console.log(err);
        })
    }).catch((err) => {
        console.log('err: ', err);
    })
}
// ! CÀI ĐẶT NGÀY
export const setDay = () => {
    let tagNgay = document.querySelector(".card__title p")
    let homNay = xuLyNgay();
    tagNgay.innerHTML =
        `
    <h6>
    ${homNay}
    </h6>
    `
        ;
}
const xuLyNgay = () => {
    let today = new Date();
    let day = today.getDate();
    let month = today.getMonth() + 1;
    let year = today.getFullYear();
    let fullDate = `Ngày ${day} Tháng ${month} Năm ${year} `;
    return fullDate;
}
// SẮP XẾP
export const sapXepAZ = () => {
    // ! lấy mảng
    batLoading();
    axios({
        url: `${BASE_URL}`,
        method: "GET"
    }).then((res) => {
        tatLoading();
        let myData = res.data;
        myData.sort((a, b) => a.name.localeCompare(b.name))
        ////!: render ra giao diện
        xuLyRender(myData);
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
}
export const sapXepZA = () => {
    batLoading();
    axios({
        url: `${BASE_URL}`,
        method: "GET"
    }).then((res) => {
        tatLoading();
        let myData = res.data;
        myData.sort((a, b) => b.name.localeCompare(a.name))
        ////!: render ra giao diện
        xuLyRender(myData);
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
}


